<p class="text-center"><img src="https://gitlab.com/mnaegeler/supernova/-/raw/c6430f6a3d1f3bafd984a96ab3f0371c47bd6ae3/app/images/supernova_4.png" width="100" alt="Supernova"></p>

[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/mnaegeler/supernova) 

# Sobre Supernova

Supernova é uma base para aplicações web que possibilita a pessoas desenvolvedoras se preocupar __apenas__ com as particularidades de seu sistema. Entregamos uma base para que você não precise se preocupar com as pequenas coisas, se precisar ainda assim será simples.

## Documentação

Veja como dar os primeiros passos na [documentação](docs/getting-started.md).

## Licença

Este código é gratuito para uso experimental ou pessoal. Para uso comercial entre em contato por <a href="mailto:marcelo.andre.naegeler@gmail.com">marcelo.andre.naegeler@gmail.com</a>.

## Icons
Icons made by <a href="https://www.flaticon.com/authors/eucalyp" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
