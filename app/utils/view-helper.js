import { labels } from '../store'

let $labels = {}

labels.subscribe(value => $labels = value)

function getLabel (labelKey) {
  if (labelKey && typeof labelKey !== 'string') {
    labelKey = labelKey.labelKey
  }

  if (!$labels[labelKey]) {
    console.warn(`Label not found. Label key: ${labelKey}`)
  }

  return $labels[labelKey] || labelKey
}

function buildURL (url) {
  let host = ''
  if (process.env.NODE_ENV === 'development') {
    host = 'http://localhost:3000'
  }

  return `${host}${url}`
}

export {
  getLabel,
  buildURL,
}
