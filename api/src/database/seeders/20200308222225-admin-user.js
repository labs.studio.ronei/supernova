'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const [enLang,] = await queryInterface.bulkInsert('Languages', [
      {
        abbr: 'en',
        name: 'English',
        active: true,
      },
      {
        abbr: 'pt_BR',
        name: 'Português',
        active: true,
      },
    ], { returning: ['id'] })

    const [sysAdmin, defaultUser] = await queryInterface.bulkInsert('Roles', [
      {
        labelKey: 'SYSTEM_ADMINISTRATOR',
        reference: 'SYSTEM_ADMINISTRATOR',
        hasFullAccess: true,
      },
      {
        labelKey: 'DEFAULT_USER',
        reference: 'DEFAULT_USER',
      },
    ], { returning: ['id'] })

    await queryInterface.bulkInsert('Users', [
      {
        id: '2cd40d9b-e4b9-41b2-9dc9-c2e9052f4cfc', // Fixed for development reasons (to not lose login after drop tables)
        name: 'Admin',
        email: 'admin@supernova.test',
        // password: admin123
        passwordHash: '$2a$08$iMgk32kr.Zzf7Y0SYgWXf.9mEcIbO.vsrJkEg.Vp.OzvL6XsMioVa',
        LanguageId: enLang.id,
        RoleId: sysAdmin.id,
      },

      {
        id: 'f9d1d052-a0a6-436d-84d1-7215633a6f2d',
        name: 'User',
        email: 'user@supernova.test',
        // password: user123
        passwordHash: '$2a$08$sMZzDHrqzWI9FMAVuFlrq.QiGGhPXXKN5euTnbSvhWEkC6pw6pGQm',
        LanguageId: enLang.id,
        RoleId: defaultUser.id,
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, { truncate: true });
  }
};
