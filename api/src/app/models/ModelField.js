module.exports = (sequelize, DataTypes) => {
  const ModelField = sequelize.define("ModelField", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    targetModel: DataTypes.STRING,
    targetModelAlias: DataTypes.STRING,
    default: DataTypes.STRING,
    name: DataTypes.STRING,
    selectOptions: DataTypes.JSON,
    length: DataTypes.INTEGER,
    modelFieldType: DataTypes.STRING,
    type: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ModelField.associate = (models) => {
    ModelField.belongsTo(models.Model);
    ModelField.hasMany(models.ViewField);
  };

  return ModelField;
};
