module.exports = (sequelize, DataTypes) => {
  const ViewField = sequelize.define("ViewField", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    labelKey: DataTypes.STRING,
    class: DataTypes.STRING,
    mask: DataTypes.STRING,
    format: DataTypes.STRING,
    valueColumn: DataTypes.STRING,
    descriptionColumn: DataTypes.STRING,
    findType: DataTypes.STRING,
    required: DataTypes.BOOLEAN,
    canUpdate: DataTypes.BOOLEAN,
    canOrderBy: DataTypes.BOOLEAN,
    breakAfter: DataTypes.BOOLEAN,
    order: DataTypes.STRING,
    filters: DataTypes.JSON,
    include: DataTypes.JSON,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ViewField.associate = (models) => {
    ViewField.belongsTo(models.View);
    ViewField.belongsTo(models.ModelField, { foreignKey: 'ModelFieldId' });
    ViewField.belongsTo(models.View, { foreignKey: 'ChildViewId', as: 'ChildView' });
  };

  return ViewField;
};
