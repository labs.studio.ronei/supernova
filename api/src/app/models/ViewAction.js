module.exports = (sequelize, DataTypes) => {
  const ViewAction = sequelize.define("ViewAction", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    class: DataTypes.STRING,
    labelKey: DataTypes.STRING,
    order: DataTypes.STRING,
    params: DataTypes.JSON,
    slug: DataTypes.STRING,
    position: DataTypes.STRING,
    type: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ViewAction.associate = (models) => {
    ViewAction.belongsTo(models.View);
  };

  return ViewAction;
};
