const fs = require('fs').promises
const util = require('util')
const path = require('path')
const { Op } = require('sequelize')

let models = require('../models')
const { User, Model, View, ViewField, ViewAction, ViewRowAction } = models
const { exportMetadata } = require('../../generators/export')

const exec = util.promisify(require('child_process').exec);

const modelsPath = path.resolve(__dirname, '..', 'models')

const publicUploadFolder = path.resolve(__dirname, '../../..', 'public', 'uploads')
const publicUploadPath = '/public/uploads'

const metaPath = path.resolve(__dirname, '..', 'metadata')

function camelToSnake (str) {
  return str.replace(/[A-Z]/g, (letter, index) => `${index > 0 ? '_' : ''}${letter.toLowerCase()}`)
}

async function getModelMetadata (modelName) {
  return await Model.findOne({
    where: {
      modelName,
    },
    include: 'ModelFields'
  })
}

async function getViewMetadata () {
  let whereIsNotAdmin = null
  /*
  if (isAdmin === false) {
    whereIsNotAdmin = {
      modelName: {
        [Op.notIn]: ['Model', 'View', 'User', 'ModelField', 'ViewField', 'ViewAction', 'ViewRowAction'],
      }
    }
  }
  */

  return await View.findAll({
    attributes: ['id', 'key', 'labelKey', 'type', 'slug', 'showOnMenu', 'filters', 'include'],
    include: [
      {
        model: models['Model'],
        where: whereIsNotAdmin,
      },

      {
        model: models['ViewField'],
        attributes: { exclude: ['createdAt', 'updatedAt', 'ModelFieldId', 'ViewId'] },
        order: [['order', 'ASC']],
        include: [
          {
            model: models['ModelField'],
            attributes: { exclude: ['createdAt', 'updatedAt', 'ModelFieldId', 'ViewId'] },
          },
        ],
      },

      {
        model: models['ViewAction'],
        attributes: ['id', 'type', 'labelKey', 'class', 'slug', 'params', 'order', 'position'],
        order: [['order', 'ASC']],
      },

      {
        model: models['ViewRowAction'],
        attributes: ['id', 'type', 'labelKey', 'class', 'slug', 'params', 'order', 'position'],
        order: [['order', 'ASC']],
      },
    ],
  })
}

async function generateModelFile (modelInstance, body, isCreating=false) {
  const modelFields = body['ModelFields'].filter(f => !['createdAt', 'updatedAt', 'deletedAt'].includes(f.name))
  
  // Create default views
  if (isCreating === true) {
    const slugName = camelToSnake(modelInstance.modelName)

    const listView = await View.create({
      ModelId: modelInstance.id,
      key: `list-${slugName}`,
      labelKey: modelInstance.modelName,
      type: 'ListView',
      slug: `/${slugName}`,
      showOnMenu: true,
      filters: null,
      include: null,
    })

    await ViewAction.create({
      ViewId: listView.id,
      labelKey: 'create',
      class: 'is-link',
      type: 'navigation',
      slug: `/${slugName}_form`,
      order: 1,
      position: 'top right',
    })

    await ViewRowAction.bulkCreate([
      {
        ViewId: listView.id,
        labelKey: 'edit',
        class: 'is-link',
        type: 'navigation',
        slug: `/${slugName}_form/:id:`,
        order: 1,
      },
      {
        ViewId: listView.id,
        labelKey: 'remove',
        class: 'has-text-danger',
        type: 'delete',
        order: 2,
        params: { id: ":id:" },
      }
    ])

    const formView = await View.create({
      ModelId: modelInstance.id,
      key: `form-${slugName}`,
      labelKey: modelInstance.modelName,
      type: 'FormView',
      slug: `/${slugName}_form`,
      showOnMenu: false,
      filters: null,
      include: null,
    })

    await ViewAction.bulkCreate([
      {
        ViewId: formView.id,
        labelKey: 'back',
        class: 'is-link is-light',
        type: 'navigation',
        slug: `/${slugName}`,
        order: 1,
        position: 'top left',
      },
      {
        ViewId: formView.id,
        labelKey: 'save',
        class: 'is-link',
        type: 'submit',
        slug: `/${slugName}`,
        order: 1,
        position: 'form end',
      }
    ])

    const fieldsForm = []
    const fieldsList = []
    modelFields.forEach(async (field, index) => {
      if (index < 4) {
        fieldsList.push({
          ModelFieldId: field.id,
          order: index,
          class: 'is-2',
          labelKey: field.name,
        })
      }

      fieldsForm.push({
        ModelFieldId: field.id,
        order: index,
        class: 'is-2',
        labelKey: field.name,
      })
    })

    const createdAt = body['ModelFields'].find(f => f.name === 'createdAt')
    const updatedAt = body['ModelFields'].find(f => f.name === 'updatedAt')

    const dateFields = [
      {
        ModelFieldId: createdAt.id,
        order: modelFields.length,
        class: 'is-2',
        canUpdate: false,
        labelKey: 'createdAt',
      },
      {
        ModelFieldId: updatedAt.id,
        order: modelFields.length + 1,
        class: 'is-2',
        canUpdate: false,
        labelKey: 'updatedAt',
      },
    ]

    const listFields = [...fieldsList, ...dateFields].map(f => {
      let nf = { ...f }
      nf.ViewId = listView.id
      return nf
    })

    const formFields = [...fieldsForm, ...dateFields].map(f => {
      let nf = { ...f }
      nf.ViewId = formView.id
      return nf
    })

    try {
      await ViewField.bulkCreate([...listFields, ...formFields])

      await exportMetadata()
      await exec('node ./src/generators/models.js')
      await exec('node ./src/generators/syncDatabase.js')

      const filePath = path.resolve(modelsPath, `${body.modelName}.js`)

      models[modelInstance.modelName] = models.sequelize['import'](filePath)
      if (models[modelInstance.modelName].associate) {
        models[modelInstance.modelName].associate(models)
      }
    } catch (e) {console.error(e)}
  }
}

const magicController = {
  async getMeta (req, res) {
    const user = await User.findByPk(req.userId, { include: models['Language'] })
    let languageAbbr = 'en'

    if (user && user.Language) {
      languageAbbr = user.Language.abbr
    } else if (req.get('Language')) {
      languageAbbr = req.get('Language')
    }

    let labels = null
    let error = ''
    try {
      const jsonPath = path.resolve(__dirname, '../..', 'labels', `${languageAbbr}.json`)
      labels = await fs.readFile(jsonPath, 'utf8')
    } catch (e) {
      error = `Translation file for language ${languageAbbr} not found`
    }

    if (!error) {
      try {
        labels = JSON.parse(labels)
      } catch (e) {
        error = `Translation file for language ${languageAbbr} has syntax errors`
      }
    }

    if (error) {
      console.error(error)
      return res.status(500).send({ messages: [error] })
    }

    try {
      const views = await getViewMetadata()

      return res.status(200).send({ views, labels, user })
    } catch (e) {
      console.error(e)
      return res.status(500).send(e)
    }
  },

  async browse (req, res) {
    const include = req.query.include
    const modelName = req.get('Model')
    let whereQuery = req.query.where
    try {
      whereQuery = JSON.parse(whereQuery)['AND']
    } catch (e) {
      whereQuery = null
    }

    if (!modelName || !models[modelName]) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    let where = null
    if (whereQuery) {
      where = {}
      whereQuery.forEach(clause => {
        if (clause.value) {
          where[clause.name] = {
            [Op[`${clause.criteria}`]]: clause.value
          }
        }
      })
    }

    try {
      const result = await models[modelName].findAll({ include, where, order: ['updatedAt'] })

      return res.status(200).send({ result })
    } catch (error) {
      console.log(error)
      return res.status(500).send({ error: error.message })
    }
  },

  async show (req, res) {
    const id = req.params.id
    const include = req.query.include
    const modelName = req.get('Model')

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    let result = null
    try {
      result = await models[modelName].findByPk(id, { include })
    } catch (e) { console.error(e) }

    if (!result) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    return res.status(200).send({ result })
  },

  async store (req, res) {
    const modelName = req.get('Model')
    const body = req.body

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    const modelMeta = await getModelMetadata(modelName)

    let element = null
    try {
      element = await models[modelName].create(body)

      const bodyToCreate = { ...body }
      Object.keys(bodyToCreate).forEach(async key => {
        const metaField = modelMeta.ModelFields.find(m => m.name === key)

        // hasMany
        if (Array.isArray(bodyToCreate[key]) === true && metaField.type === 'hasMany') {
          if (modelName === 'Model') {
            bodyToCreate[key].push({ name: 'createdAt', type: 'datetime', modelFieldType: 'DataTypes.DATE' })
            bodyToCreate[key].push({ name: 'updatedAt', type: 'datetime', modelFieldType: 'DataTypes.DATE' })
            bodyToCreate[key].push({ name: 'deletedAt', type: 'datetime', modelFieldType: 'DataTypes.DATE' })
          }

          bodyToCreate[key].forEach(async (arrayItem, index) => {
            let item = await models[metaField.targetModel].upsert({ ...arrayItem }, { returning: true })
            await element[`add${key}`](item[0])
            bodyToCreate[key][index].id = item[0].id
          })
        }
      })
    } catch (e) {
      console.error(e)
      return res.status(500).send({ messages: [e.message] })
    }

    if (modelName === 'Model') {
      try {
        await generateModelFile(element, body, true)
      } catch (e) {
        return res.status(500).send({ messages: [e.message] })
      }
    }

    return res.status(200).send({ id: element.id })
  },

  async update (req, res) {
    const id = req.params.id
    const modelName = req.get('Model')
    const body = req.body

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    let element = null
    try {
      element = await models[modelName].findByPk(id)
    } catch (e) { console.error(e) }

    if (!element) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    const modelMeta = await getModelMetadata(modelName)

    const bodyToUpdate = { ...body }
    Object.keys(bodyToUpdate).forEach(async key => {
      const metaField = modelMeta.ModelFields.find(m => m.name === key)

      // hasMany
      if (Array.isArray(bodyToUpdate[key]) === true && metaField.type === 'hasMany') {
        let relationsToRemove = await element[`get${key}`]()

        bodyToUpdate[key] = bodyToUpdate[key].map(async arrayItem => {
          if (arrayItem.id) {
            // If the relation still exists, remove from removal list
            const index = relationsToRemove.findIndex(r => r.id === arrayItem.id)
            if (index !== -1) {
              relationsToRemove.splice(index, 1)
            }
          }

          let item = await models[metaField.targetModel].upsert({ ...arrayItem }, { returning: true })
          await element[`add${key}`](item[0])
          return item[0]
        })

        relationsToRemove.forEach(async rel => {
          await rel.destroy()
        })
      }

      if (metaField.type === 'file') {
        const file = body[key].split(';')
        const mime = file[0]
        const data = file[1]
        const name = `${+ new Date()}`// need to change it
        const filename = `${name}.${mime.split('/')[1]}`
        const filepath = `${publicUploadFolder}/${filename}`
        const fileToSave = `${publicUploadPath}/${filename}`

        body[key] = fileToSave

        try {
          let buffer = Buffer.from(data.split(',')[1], 'base64')
          await fs.mkdir(publicUploadFolder, { recursive: true })
          await fs.writeFile(filepath, buffer)
        } catch (e) {
          console.log(e)
        }
      }
    })

    try {
      await element.update(body)
    } catch (e) {
      return res.status(500).send({ messages: [e.message] })
    }

    if (modelName === 'Model') {
      /*
      try {
        await generateModelFile(element, bodyToUpdate, false)
      } catch (e) {
        return res.status(500).send({ messages: [e.message] })
      }
      */
    }

    return res.status(200).send({ id: element.id })
  },

  async delete (req, res) {
    const id = req.params.id
    const modelName = req.get('Model')

    if (!modelName) {
      return res.status(400).send({ messages: ['Model not provided'] })
    }

    let result = null
    try {
      result = await models[modelName].findByPk(id)
    } catch (e) {
      console.error(e)
    }

    if (!result) {
      const model = await Model.findOne({ where: { modelName } })

      return res.status(404).send({ messages: [`${model.name} not found`] })
    }

    try {
      await result.destroy()
    } catch (e) {
      console.error(e)
      return res.status(500).send({ messages: ['Error destroying object'] })
    }

    return res.status(200).send()
  }
}

module.exports = magicController
